﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Distance : MonoBehaviour
{

    public Transform ball;
    public Transform goal;
    public Transform player;
    public TextMeshProUGUI distanceText;
    public float panelHeight;
    public float panelDistance;
    float distance;
    Vector3 direction;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        distance = (goal.position - ball.position).magnitude;
        distanceText.text = distance.ToString() + " m";
        direction = goal.position - player.position;
        transform.position = player.position + panelDistance * direction.normalized + panelHeight * Vector3.up;
        transform.LookAt(transform.position + new Vector3 (direction.x, 0, direction.z));

    }
}
