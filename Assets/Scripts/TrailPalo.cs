﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailPalo : MonoBehaviour
{
    public TrailRenderer trail;
    public Transform palo;

    // Start is called before the first frame update
    void Start()
    {
        trail.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger) || Input.GetKeyDown(KeyCode.Space))
        {
            trail.enabled = true;

            trail.transform.parent = palo;
            trail.Clear();
        }
        if (OVRInput.GetUp(OVRInput.Button.SecondaryIndexTrigger) || Input.GetKeyUp(KeyCode.Space))
        {
            trail.transform.parent = null;
        }
    }
}
