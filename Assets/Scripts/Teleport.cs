﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public Transform player;
    public Transform goal;
    public Transform bolsa;
    float speed;
    Vector3 goalDirection;
    Vector3 goalDirectionUnit;
    Vector3 ballLeftDirection;
    public Vector3 lastPosition;
    Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        speed = rb.velocity.sqrMagnitude;
        if (speed < 0.001f && speed > 0.0001f)
            lastPosition = transform.position;
    }

    public void DoTeleport()
    {
        goalDirection = (goal.position - transform.position);
        goalDirectionUnit = goalDirection / goalDirection.magnitude;
        ballLeftDirection = Vector3.Cross(goalDirectionUnit, Vector3.up);
        player.position = transform.position + ballLeftDirection + Vector3.up * 1.30f;
        bolsa.position = transform.position + 2 * ballLeftDirection;
        
    }
}
