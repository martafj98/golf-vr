﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cabeza : MonoBehaviour
{

    Vector3 lastPosition = Vector3.zero;
    Vector3 direction, velocity;
    float myMass;
    public static bool shot = false;

    // Start is called before the first frame update
    void Start()
    {
        myMass = GetComponent<Rigidbody>().mass;
    }

    // Update is called once per frame
    void Update()
    {
        
        direction = transform.position - lastPosition;
        velocity = direction / Time.deltaTime;
        lastPosition = transform.position;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            float ballMass = col.GetComponent<Rigidbody>().mass;
            col.GetComponent<Rigidbody>().velocity = (velocity * 2 * myMass) / (myMass + ballMass);
            shot = true;
            print("Velocidad del palo: " + velocity);
            print("Velocidad resultante: " + col.GetComponent<Rigidbody>().velocity);
        }
    }
}
